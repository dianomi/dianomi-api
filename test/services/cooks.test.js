const assert = require('assert');
const app = require('../../src/app');

describe('\'cooks\' service', () => {
  it('registered the service', () => {
    const service = app.service('cooks');

    assert.ok(service, 'Registered the service');
  });
});
