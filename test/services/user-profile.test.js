const assert = require('assert');
const app = require('../../src/app');

describe('\'user-profile\' service', () => {
  it('registered the service', () => {
    const service = app.service('users/me');

    assert.ok(service, 'Registered the service');
  });
});
