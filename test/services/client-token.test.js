const assert = require('assert');
const app = require('../../src/app');

describe('\'client-token\' service', () => {
  it('registered the service', () => {
    const service = app.service('client-token');

    assert.ok(service, 'Registered the service');
  });
});
