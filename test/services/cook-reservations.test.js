const assert = require('assert');
const app = require('../../src/app');

describe('\'cook-reservations\' service', () => {
  it('registered the service', () => {
    const service = app.service('cooks/:id/reservations');

    assert.ok(service, 'Registered the service');
  });
});
