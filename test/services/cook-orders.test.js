const assert = require('assert');
const app = require('../../src/app');

describe('\'cook-orders\' service', () => {
  it('registered the service', () => {
    const service = app.service('cooks/:id/orders');

    assert.ok(service, 'Registered the service');
  });
});
