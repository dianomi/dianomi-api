const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const populateReservations = require('../../src/hooks/populate-reservations');

describe('\'populate-reservations\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: populateReservations()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
