const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const addReservationUser = require('../../src/hooks/add-reservation-user');

describe('\'add-reservation-user\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: addReservationUser()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
