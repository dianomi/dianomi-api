const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const braintreeRequest = require('../../src/hooks/braintree-request');

describe('\'braintree-request\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      after: braintreeRequest()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
