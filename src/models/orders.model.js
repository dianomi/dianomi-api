// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient');
  const orders = sequelizeClient.define(
    'orders',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      details: {
        type: DataTypes.STRING
      },
      deliverable: {
        type: DataTypes.BOOLEAN,
        allowNull: false
      },
      status: {
        type: DataTypes.STRING,
        default: 'pending',
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  orders.associate = function(models) {
    orders.belongsTo(models.users, {
      foreignKey: 'userId'
    });
    orders.belongsTo(models.products, {
      foreignKey: 'productId'
    });
    orders.belongsTo(models.cooks, {
      foreignKey: 'cookId'
    });
  };

  return orders;
};
