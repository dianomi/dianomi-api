// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient');
  const users = sequelizeClient.define(
    'users',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      phoneNumber: {
        type: DataTypes.STRING,
        default: ''
      },
      name: {
        type: DataTypes.STRING,
        default: ''
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  users.associate = function(models) {
    users.hasOne(models.cooks, {
      foreignKey: 'userId'
    });
    users.hasOne(models.addresses, {
      foreignKey: 'userId'
    });
    users.hasMany(models.orders, {
      foreignKey: 'userId'
    });
    users.hasMany(models.reservations, {
      foreignKey: 'userId'
    });
  };

  return users;
};
