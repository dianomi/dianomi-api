// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const cooks = sequelizeClient.define('cooks', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    description: DataTypes.STRING,
    priceLevel: DataTypes.STRING,
    subcriptionId: DataTypes.STRING,
    status: DataTypes.STRING,
    userId: {
      type: DataTypes.UUID
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  cooks.associate = function (models) {
    cooks.belongsTo(models.users, {
      foreignKey: 'userId'
    });
    cooks.hasMany(models.products, {
      foreignKey: 'cookId'
    });
    cooks.hasMany(models.orders, {
      foreignKey: 'cookId'
    });
    cooks.hasMany(models.reservations, {
      foreignKey: 'cookId'
    });
  };

  return cooks;
};
