// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient');
  const reservations = sequelizeClient.define(
    'reservations',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      reservationDate: {
        type: DataTypes.DATE,
        allowNull: false
      },
      reservationSize: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      status: {
        type: DataTypes.STRING,
        default: 'pending'
      },
      cookId: {
        type: DataTypes.UUID
      },
      userId: {
        type: DataTypes.UUID
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  reservations.associate = function(models) {
    reservations.belongsTo(models.users, {
      foreignKey: 'userId'
    });
    reservations.belongsTo(models.cooks, {
      foreignKey: 'cookId'
    });
  };

  return reservations;
};
