// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function(options = {}) {
  return async context => {
    const { result, app } = context;
    const data = {
      email: result.email,
      password: result.password,
      strategy: 'local'
    };

    const params = { data, payload: { userId: result.id } };
    const { accessToken } = await app
      .service('authentication')
      .create({}, params);
    result.accessToken = accessToken;

    return context;
  };
};
