// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function(options = {}) {
  return async context => {
    const { params, app, result } = context;
    await app
      .service('cooks')
      .create({
        subcriptionId: result.id,
        userId: params.user.id,
        status: result.status,
        description: result.details
      });
    return context;
  };
};
