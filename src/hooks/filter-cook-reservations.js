// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function(options = {}) {
  return async context => {
    if (context.params.route.id) {
      context.params.query.cookId = context.params.route.id;
    }
    return context;
  };
};
