// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    await Promise.all(context.result.data.map(
      async item => {
        item.cook = await context.app.service('cooks').find({}, { cookId: item.cookId });
      }
    ));
    return context;
  };
};
