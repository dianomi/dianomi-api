// Initializes the `cook-reservations` service on path `/cooks/:id/reservations`
const createService = require('feathers-sequelize');
const hooks = require('./cook-reservations.hooks');
const createModel = require('../../models/reservations.model');

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/cooks/:id/reservations', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('cooks/:id/reservations');

  service.hooks(hooks);
};
