const users = require('./users/users.service.js');
const orders = require('./orders/orders.service.js');
const products = require('./products/products.service.js');
const addresses = require('./addresses/addresses.service.js');
const categories = require('./categories/categories.service.js');
const reservations = require('./reservations/reservations.service.js');
const payments = require('./payments/payments.service.js');
const cooks = require('./cooks/cooks.service.js');
const cookReservations = require('./cook-reservations/cook-reservations.service.js');
const cookOrders = require('./cook-orders/cook-orders.service.js');
const userProfile = require('./user-profile/user-profile.service.js');

const clientToken = require('./client-token/client-token.service.js');

// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(orders);
  app.configure(products);
  app.configure(addresses);
  app.configure(categories);
  app.configure(reservations);
  app.configure(payments);
  app.configure(cooks);
  app.configure(cookReservations);
  app.configure(cookOrders);
  app.configure(userProfile);
  app.configure(payments);
  app.configure(clientToken);
};
