// Initializes the `client-token` service on path `/client-token`
const createService = require('./client-token.class.js');
const hooks = require('./client-token.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/client-token', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('client-token');

  service.hooks(hooks);
};
