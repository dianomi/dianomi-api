// Initializes the `products` service on path `/bussinesses/:id/products`
const createService = require('feathers-sequelize');
const createModel = require('../../models/products.model');
const hooks = require('./products.hooks');

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/cooks/:id/products', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('cooks/:id/products');

  service.hooks(hooks);
};
