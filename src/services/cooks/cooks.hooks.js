const { authenticate } = require('@feathersjs/authentication').hooks;

const populateCooks = require('../../hooks/populate-cooks');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [populateCooks()],
    get: [populateCooks()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
