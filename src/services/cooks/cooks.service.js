// Initializes the `cooks` service on path `/cooks`
const createService = require('feathers-sequelize');
const createModel = require('../../models/cooks.model');
const hooks = require('./cooks.hooks');

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/cooks', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('cooks');

  service.hooks(hooks);
};
