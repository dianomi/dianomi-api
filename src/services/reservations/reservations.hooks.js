const { authenticate } = require('@feathersjs/authentication').hooks;

const addReservationUser = require('../../hooks/add-reservation-user');

const filterReservations = require('../../hooks/filter-reservations');

const populateReservations = require('../../hooks/populate-reservations');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [filterReservations()],
    get: [filterReservations()],
    create: [addReservationUser()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [populateReservations()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
