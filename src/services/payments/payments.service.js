// Initializes the `payments` service on path `/payments`
const createService = require('./payments.class.js');
const hooks = require('./payments.hooks');

module.exports = function(app) {
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/payments', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('payments');

  service.hooks(hooks);
};
