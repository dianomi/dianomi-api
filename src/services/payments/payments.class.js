/* eslint-disable no-unused-vars */
const braintree = require('braintree');

const gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: '24bgdt2hdyk2c54j',
  publicKey: '6xd8744wp8crkj2k',
  privateKey: 'a4a054544268718b675bce6b1d83d5fb'
});

class Service {
  constructor(options) {
    this.options = options || {};
    this.gateway = gateway;
  }

  async find(params) {
    return [];
  }

  async get(id, params) {
    return {
      id,
      text: `A new message with ID: ${id}!`
    };
  }

  async create(data, params) {
    const [firstName, lastName] = params.user.name.split(' ');
    const response = await this.gateway.customer.create({
      firstName,
      lastName,
      paymentMethodNonce: data.paymentNonce
    });
    const subscriptionResponse = await this.gateway.subscription.create({
      paymentMethodToken: response.customer.paymentMethods[0].token,
      planId: '6m8m'
    });

    subscriptionResponse.subscription.details = data.details;

    return subscriptionResponse.subscription;
  }

  async update(id, data, params) {
    return data;
  }

  async patch(id, data, params) {
    return data;
  }

  async remove(id, params) {
    return { id };
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
