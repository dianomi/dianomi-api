// Initializes the `user-profile` service on path `/users/me`
const createService = require('./user-profile.class.js');
const hooks = require('./user-profile.hooks');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/user/me', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('user/me');

  service.hooks(hooks);
};
