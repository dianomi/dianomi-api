const { authenticate } = require('@feathersjs/authentication').hooks;

const { protect } = require('@feathersjs/authentication-local').hooks;

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: []
  },

  after: {
    all: [protect('password')],
    find: []
  },

  error: {
    all: [],
    find: []
  }
};
