const { authenticate } = require('@feathersjs/authentication').hooks;

const filterAddresses = require('../../hooks/filter-addresses');

const addAddressUser = require('../../hooks/add-address-user');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [filterAddresses()],
    get: [filterAddresses()],
    create: [addAddressUser()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
