// Initializes the `addresses` service on path `/users/:id/addresses`
const createService = require('feathers-sequelize');
const createModel = require('../../models/addresses.model');
const hooks = require('./addresses.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/users/:id/addresses', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('users/:id/addresses');

  service.hooks(hooks);
};
