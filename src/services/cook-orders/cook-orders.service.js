// Initializes the `cook-orders` service on path `/cooks/:id/orders`
const createService = require('feathers-sequelize');
const hooks = require('./cook-orders.hooks');
const createModel = require('../../models/orders.model');

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/cooks/:id/orders', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('cooks/:id/orders');

  service.hooks(hooks);
};
