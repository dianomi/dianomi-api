const { authenticate } = require('@feathersjs/authentication').hooks;

const populateOrders = require('../../hooks/populate-orders');

module.exports = {
  before: {
    all: [authenticate('jwt'), populateOrders()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
